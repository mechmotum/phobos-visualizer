﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using System.IO.Ports;
using System.IO;

public class SerialDeviceSelectionDialog : MonoBehaviour {

	[SerializeField] Toggle detectionToggle;
	[SerializeField] Dropdown detectionDropdown;
	[SerializeField] Toggle manualToggle;
	[SerializeField] InputField manualInputField;

	public void OpenDialog() {
		RefreshDevices();
		gameObject.SetActive(true);
	}

	public void CloseDialog() {
		gameObject.SetActive(false);
	}

	public void RefreshDevices() {
		string[] options = SerialDeviceSelectionDialog.DetectDevices(Application.platform);

		detectionDropdown.ClearOptions();

		if (options.Length == 0) {
			DisableDetection();
		} else {
			detectionDropdown.AddOptions(new List<string>(options));
			EnableDetection();
		}
	}

	public string GetSerialDevicePath() {
		return detectionToggle.isOn
			? detectionDropdown.options[detectionDropdown.value].text
			: manualInputField.text;
	}

	void DisableDetection() {
		detectionToggle.interactable = false;
		detectionDropdown.interactable = false;
		detectionToggle.isOn = false;
		manualToggle.isOn = true;
	}

	void EnableDetection() {
		detectionToggle.interactable = true;
		detectionDropdown.interactable = true;
		manualToggle.isOn = false;
		detectionToggle.isOn = true;
	}

	static string[] DetectDevices(RuntimePlatform platform) {
		switch (Application.platform) {
			case RuntimePlatform.LinuxEditor: // Fall through.
			case RuntimePlatform.LinuxPlayer: return Directory.GetFiles("/dev", "ttyACM*");
			case RuntimePlatform.OSXEditor: // Fall through.
			case RuntimePlatform.OSXPlayer: return Directory.GetFiles("/dev", "tty.usbmodem*");
			case RuntimePlatform.WindowsEditor: // Fall through.
			case RuntimePlatform.WindowsPlayer: return SerialPort.GetPortNames();
		}
		return new string[] {};
	}
}

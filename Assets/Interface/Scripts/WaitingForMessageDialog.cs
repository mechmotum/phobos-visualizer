﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using System.IO.Ports;
using System.IO;

public class WaitingForMessageDialog : MonoBehaviour {
	[SerializeField] Text description;

	public void OpenDialog(string serialDevicePath) {
		description.text = "We are waiting on a message on serial device \"" + serialDevicePath + "\" from the microcontroller. Make sure it is connected.";
		gameObject.SetActive(true);
	}

	public void CloseDialog() {
		gameObject.SetActive(false);
	}
}

﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class ConfigurationMenu : MonoBehaviour {

	[SerializeField]
	Toggle serialDevicePortDetectedToggle;

	[SerializeField]
	Dropdown serialDevicePortDetectedDropdown;

	[SerializeField]
	Toggle serialDevicePortManualToggle;
	
	[SerializeField]
	InputField serialDevicePortManualInputField;
	
	void Open() {

	}

	void Apply() {

	}
}

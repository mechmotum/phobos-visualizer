﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class ErrorDialog : MonoBehaviour {
	[SerializeField] Text title;
	[SerializeField] Text description;

	public void OpenDialog(string title, string description) {
		this.title.text = title;
		this.description.text = description;
		gameObject.SetActive(true);
	}

	public void CloseDialog() {
		gameObject.SetActive(false);
	}
}
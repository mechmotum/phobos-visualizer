﻿using UnityEngine;
using UnityEngine.UI;
using UnityEngine.EventSystems;
using System;

class TooltipController : MonoBehaviour, IPointerEnterHandler, IPointerExitHandler {
	[SerializeField]
	GameObject target;

    void IPointerEnterHandler.OnPointerEnter(PointerEventData eventData) {
		target.SetActive(true);
    }

    void IPointerExitHandler.OnPointerExit(PointerEventData eventData) {
		target.SetActive(false);
    }
}
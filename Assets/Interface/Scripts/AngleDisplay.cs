﻿using System.Collections;
using System.Collections.Generic;
using Units.Angles;
using UnityEngine;
using UnityEngine.UI;

[ExecuteInEditMode]
public class AngleDisplay : MonoBehaviour {

	[SerializeField] Text valueText;

	[SerializeField] Text unitText;

	[SerializeField] Image image;

	public AngleUnit unit { get; set; }

    [SerializeField] float value;

	public enum AngleUnit {
		Degrees,
		Radians,
        Revolutions,
	}

    public void RotateUnits() {
        switch (unit) {
            case AngleUnit.Degrees: SetUnit(AngleUnit.Radians); break;
            case AngleUnit.Radians: SetUnit(AngleUnit.Revolutions); break;
            case AngleUnit.Revolutions: SetUnit(AngleUnit.Degrees); break;
        }
    }

	public void SetValue<TAngularUnit>(TAngularUnit angle) where TAngularUnit : IAngularUnit {
        switch (unit) {
            case AngleUnit.Degrees: this.value = (float) angle.ToDegrees; break;
            case AngleUnit.Radians: this.value = (float) angle.ToRadians; break;
            case AngleUnit.Revolutions: this.value = (float) angle.ToRevolutions; break;
        }
        UpdateUI();
	}

    public void SetUnit(AngleUnit unit) {
        AngleUnit oldUnit = this.unit;
        this.unit = unit;
        switch (oldUnit) {
            case AngleUnit.Radians: SetValue((Radians) value); break;
            case AngleUnit.Degrees: SetValue((Degrees) value); break;
            case AngleUnit.Revolutions: SetValue((Revolutions) value); break;
        }
    }

    public void UpdateUI() {
        float wrappedValue;
        float wrappedRevolutions;
        string valueFormat;
        string unitString;
		switch (unit) {
			case AngleUnit.Radians: {
                Radians radians = this.value;
                Radians wrappedRadians = radians % Constants.RadiansPerRevolution;
                wrappedValue = (float) wrappedRadians;
                wrappedRevolutions = (float) (Revolutions) wrappedRadians;
                valueFormat = "{0:F2}";
                unitString = "rad";
                break;
            }
			case AngleUnit.Degrees: {
                Degrees degrees = this.value;
                Degrees wrappedDegrees = degrees % Constants.DegreesPerRevolution;
                wrappedValue = (float) wrappedDegrees;
                wrappedRevolutions = (float) (Revolutions) wrappedDegrees;
                valueFormat = "{0:000}";
                unitString = "deg";
                break;
            }
            case AngleUnit.Revolutions: {
                Revolutions revolutions = this.value;
                Revolutions wrappedRevolutions_ = revolutions % 1;
                wrappedValue = (float) wrappedRevolutions_;
                wrappedRevolutions = (float) wrappedRevolutions_;
                valueFormat = "{0:F2}";
                unitString = "rev";
                break;
            }
            default: throw new System.NotImplementedException();
        }
        if (wrappedRevolutions >= 0) {
            image.fillAmount = wrappedRevolutions;
            image.fillClockwise = true;
        } else {
            image.fillAmount = -wrappedRevolutions;
            image.fillClockwise = false;
        }
        valueText.text = string.Format(valueFormat, wrappedValue);
        unitText.text = unitString;
    } 

    public void Update() {
        UpdateUI();
    }
}

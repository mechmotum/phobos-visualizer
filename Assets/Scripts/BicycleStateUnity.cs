﻿using Units.Angles;
using UnityEngine;

[System.Serializable]
public class BicycleStateUnity {
	public Vector3 Position;
	public Degrees Roll;
	public Degrees Pitch;
	public Degrees Yaw;
	public Degrees Steer;
	public Degrees Wheel;

	public Quaternion Rotation {
		get {
			return Quaternion.Euler(0, (float) Yaw, 0) *
				Quaternion.Euler((float) Roll, 0, 0) *
				Quaternion.Euler(0, 0, (float) Pitch);
		}
	}

	public static implicit operator BicycleStateUnity(BicycleState state) {
		if (state == null) return null;
		
        // http://rspa.royalsocietypublishing.org/content/463/2084/1955#T1
        // X is forward, Z is down, Y is to the right. It is a right handed
        // coordinate system while Unity uses a left handed coordinate system.

        // Rotations are applied 312 Euler, so for the given coordinate system
        // that means around Z (yaw, right positive), then around X (roll, right
        // positive) and then around Y (pitch, up positive). In Unity we must
        // rotate around Y, X then Z. Quaternion.Euler rotates around Z, X then Y. 

		Terrain terrain = Terrain.activeTerrain;
		Vector3 position = new Vector3(state.X, 0, -state.Y);
		position.y = terrain.SampleHeight(position) + terrain.transform.position.y;
		
		return new BicycleStateUnity {
			Position = position,
			Roll = -state.Roll,
			// 18 deg pitch means the bicycle is horizontal.
			Pitch = (Degrees) state.Pitch - 18,
			Yaw = state.Yaw,
			Steer = -state.Steer,
			Wheel = state.Wheel,
		};
	}
}

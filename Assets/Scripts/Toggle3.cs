﻿using UnityEngine;

[System.Serializable]
public struct Toggle3 {
    [SerializeField] private bool _X;
    [SerializeField] private bool _Y;
    [SerializeField] private bool _Z;

    public bool X { get { return _X; } }
    public bool Y { get { return _Y; } }
    public bool Z { get { return _Z; } }

    public Toggle3(bool x, bool y, bool z) {
        _X = x;
        _Y = y;
        _Z = z;
    }

    public Vector3 Choose(Vector3 on, Vector3 off) {
        return new Vector3(
            X ? on.x : off.x,
            Y ? on.y : off.y,
            Z ? on.z : off.z
        );
    }
};

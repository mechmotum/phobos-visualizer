﻿using UnityEngine;
using Units.Angles;
#if UNITY_EDITOR
using UnityEditor;
#endif

public class CameraController : MonoBehaviour {
	
	[SerializeField] BicycleStateProvider bicycleStateProvider;

	[Range(0.0f, 1.0f)]
	[SerializeField] float _mountRollScaleFactor;

	public float MountRollScaleFactor {
		get { return _mountRollScaleFactor; }
		set {
			_mountRollScaleFactor = value;
#if UNITY_EDITOR
			EditorUtility.SetDirty(this);
#endif
		}
	}

	[SerializeField] Transform swivel;

	[Range(0.0f, 1.0f)]
	[SerializeField] float _swivelRollScaleFactor;

	public float SwivelRollScaleFactor {
		get { return _swivelRollScaleFactor; }
		set {
			_swivelRollScaleFactor = value;
#if UNITY_EDITOR
			EditorUtility.SetDirty(this);
#endif
		}
	}

	void OnEnable() {
		bicycleStateProvider.OnStateChange += UpdateCamera;
	}

	void OnDisable() {
		bicycleStateProvider.OnStateChange -= UpdateCamera;
	}

	void UpdateCamera(BicycleState state) {
		UpdateCamera((BicycleStateUnity) state);
	}

	void UpdateCamera(BicycleStateUnity state) {
		transform.localPosition = state.Position;

		// Compute the mount roll limit.
		Degrees mountRollLimit = _mountRollScaleFactor*180;
		// Clamp the mount roll between +- limit.
		Degrees limitedMountRoll = state.Roll;
		if (limitedMountRoll > mountRollLimit) limitedMountRoll = mountRollLimit;
		else if (limitedMountRoll < -mountRollLimit) limitedMountRoll = -mountRollLimit;
		// Scale the clamped roll.
		Degrees mountRoll = limitedMountRoll*_mountRollScaleFactor;

		// Compute the swivel roll limit.
		Degrees swivelRollLimit = _swivelRollScaleFactor*180;
		// Clamp the swivel roll between +- limit.
		Degrees limitedSwivelRoll = state.Roll;
		if (limitedSwivelRoll > swivelRollLimit) limitedSwivelRoll = swivelRollLimit;
		else if (limitedSwivelRoll < -swivelRollLimit) limitedSwivelRoll = -swivelRollLimit;
		// Scale the clamped roll.
		Degrees swivelRoll = limitedSwivelRoll*_swivelRollScaleFactor;

		Quaternion mountRollQuaternion = Quaternion.Euler((float) mountRoll, 0, 0);
		Quaternion mountPitchQuaternion = Quaternion.Euler(0, 0, (float) state.Pitch);

		transform.localRotation = Quaternion.Euler(0, (float) state.Yaw, 0)
			*mountRollQuaternion
			*mountPitchQuaternion;
		
		swivel.transform.localRotation = Quaternion.Inverse(mountPitchQuaternion)
			*Quaternion.Inverse(mountRollQuaternion)
			*Quaternion.Euler((float) swivelRoll, 0, 0);
	}
}

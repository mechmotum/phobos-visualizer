﻿using UnityEngine;

public class SimpleRotateController : MonoBehaviour {
	[SerializeField]
	Vector3 axis = Vector3.up;

	[SerializeField]
	float degreesPerSecond = 360.0f;

	void Update () {
		// Compute the time required for one full revolution.
		float secondsPerRevolution = 360.0f/degreesPerSecond;

		// Wrap the time so the float doesn't overflow when multiplying with
		// degreesPerSecond.
		float wrappedTime = Mathf.Repeat(Time.time, secondsPerRevolution);

		// Rotate along the given axis.
		transform.rotation = Quaternion.AngleAxis(wrappedTime*degreesPerSecond, axis);
	}
}

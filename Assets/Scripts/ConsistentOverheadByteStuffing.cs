public static class ConsistentOverheadByteStuffing {
    public class DecodeResult {
        public enum Status {
            OK,
            WRITE_OVERFLOW,
            READ_OVERFLOW,
            UNEXPECTED_ZERO
        }
        public Status status { get; }

        /**
        If the status is OK or UNEXPECTED_ZERO, the number of bytes that were
        read from src_start. Otherwise it is set to 0.
        */
        public int consumed { get; }

        /**
        If the status is OK, the number of bytes that were written to dst_start.
        Otherwise it is set to 0.
        */
        public int produced { get; }

        private DecodeResult(Status status, int consumed, int produced) {
            this.status = status;
            this.consumed = consumed;
            this.produced = produced;
        }

        public static DecodeResult CreateOk(int consumed, int produced) {
            return new DecodeResult(Status.OK, consumed, produced);
        }

        public static DecodeResult CreateWriteOverflow() {
            return new DecodeResult(Status.WRITE_OVERFLOW, 0, 0);
        }

        public static DecodeResult CreateReadOverflow() {
            return new DecodeResult(Status.READ_OVERFLOW, 0, 0);
        }

        public static DecodeResult CreateUnexpectedZero(int consumed) {
            return new DecodeResult(Status.UNEXPECTED_ZERO, consumed, 0);
        }
    }

    /**
    COBS decode a byte array.
    */
    public static DecodeResult decode(byte[] src, int src_start, int src_end, byte[] dst, int dst_start, int dst_end) {
        // This variable will always point to the next byte to read.
        int src_i = src_start;

        // This variable will always point to the next byte to write.
        int dst_i = dst_start;

        // Read the first offset.
        if (src_i >= src_end) {
            return DecodeResult.CreateReadOverflow();
        }
        int offset = src[src_i++];

        // If the first offset is 0x00 we can stop immediately.
        if (offset == 0x00) {
            return DecodeResult.CreateUnexpectedZero(src_i - src_start);
        }

        while (true) {
            // Compute the location of the next zero. Subtract one from
            // offset to get the number of data bytes that follow.
            int src_i_copy_end = src_i + offset - 1;
            int dst_i_copy_end = dst_i + offset - 1;

            // Check if we can copy the data until the next zero. 
            if (src_i_copy_end > src_end) {
                return DecodeResult.CreateReadOverflow();
            }
            if (dst_i_copy_end > dst_end) {
              return DecodeResult.CreateWriteOverflow();
            }

            // Copy data until we are at the next zero.
            while (src_i < src_i_copy_end) {
                byte b = src[src_i++];

                // We should not encounter a zero in the encoded data.
                if (b == 0x00) {
                    // Return control to the caller so it can restart a
                    // decoding operation from where we stop now.
                    return DecodeResult.CreateUnexpectedZero(src_i - src_start);
                }

                dst[dst_i++] = b;
            }

            // Retrieve the next zero offset.
            if (src_i >= src_end) {
                return DecodeResult.CreateReadOverflow();
            }
            int next_offset = src[src_i++];

            // Check if we've hit the end.
            if (next_offset == 0x00) break;

            // If the last offset was not equal to 0xff and we have not
            // reached the end, output a zero.
            if (offset != 0xff) {
                if (dst_i >= dst_end) {
                    return DecodeResult.CreateWriteOverflow();
                }
                dst[dst_i++] = 0x00;
            }

            // Store the offset for the next iteration.
            offset = next_offset;
        }

        return DecodeResult.CreateOk(src_i - src_start, dst_i - dst_start);
    }
}
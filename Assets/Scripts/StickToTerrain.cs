﻿using UnityEngine;

public class StickToTerrain : MonoBehaviour {

	void LateUpdate () {
		Terrain terrain = Terrain.activeTerrain;
		transform.position = new Vector3(
			transform.position.x,
			terrain.SampleHeight(transform.position) + terrain.transform.position.y,
			transform.position.z
		);
	}
}

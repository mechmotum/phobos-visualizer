﻿using UnityEngine;

public static class Configuration {
	public static string SerialDevicePath { get; set; }

	public static string GetAutomaticSerialDevicePath() {
		return SerialDevicePath != null
			? SerialDevicePath
			: GetDefaultSerialDevicePath();
	}

	public static string GetDefaultSerialDevicePath() {
		return PlatformToDefaultSerialDevicePath(Application.platform);
	}

	public static string PlatformToDefaultSerialDevicePath(RuntimePlatform platform) {
		switch (platform) {
			case RuntimePlatform.LinuxEditor: // Fall through.
			case RuntimePlatform.LinuxPlayer: return "/dev/ttyACM0";
			case RuntimePlatform.OSXEditor: // Fall through.
			case RuntimePlatform.OSXPlayer: return "/dev/tty.usbmodem311";
		}
		return null;
	}
}

using UnityEngine;
using UnityEngine.SceneManagement;
using System;
using pb;

class LiveScene : MonoBehaviour {
    [SerializeField] BicycleStateProvider bicycleStateProvider;

    [SerializeField] SerialDeviceSelectionDialog serialDeviceSelectionDialog;

    [SerializeField] WaitingForMessageDialog waitingForMessageDialog;

    [SerializeField] ErrorDialog errorDialog;

    SerialThread thread = null;

    public GameState State { get; private set; }

    public enum GameState {
        SelectingSerialDevicePath,
        WaitingForMessage,
        Running,
        WaitingForErrorConfirm
    }

    void Awake() {
        State = GameState.SelectingSerialDevicePath;
        serialDeviceSelectionDialog.OpenDialog();
    }

    public void Connect() {
        if (State != GameState.SelectingSerialDevicePath) {
            throw new UnityException("Expected game state to be SelectingSerialDevicePath");
        }
        string serialDevicePath = serialDeviceSelectionDialog.GetSerialDevicePath();
        serialDeviceSelectionDialog.CloseDialog();
        thread = new SerialThread(serialDevicePath);
        State = GameState.WaitingForMessage;
        waitingForMessageDialog.OpenDialog(serialDevicePath);
    }

    void StopAndWaitForSerialThread() {
        if (thread == null) return;
        thread.SignalStop();
        // Freezes up the UI.
        thread.WaitUntilStopped();
        thread = null;
    }

    public void CancelConnect() {
        if (State != GameState.WaitingForMessage) {
            throw new UnityException("Expected game state to be WaitingForMessage");
        }
        StopAndWaitForSerialThread();
        waitingForMessageDialog.CloseDialog();
        State = GameState.SelectingSerialDevicePath;
        serialDeviceSelectionDialog.OpenDialog();
    }

    public void DisplayConnectError(Exception exception) {
        if (State != GameState.WaitingForMessage) {
            throw new UnityException("Expected game state to be WaitingForMessage");
        }
        StopAndWaitForSerialThread();
        waitingForMessageDialog.CloseDialog();
        State = GameState.WaitingForErrorConfirm;
        errorDialog.OpenDialog("Connection Failure", exception.Message + exception.StackTrace);
    }

    public void ConfirmError() {
        if (State != GameState.WaitingForErrorConfirm) {
            throw new UnityException("Expected game state to be WaitingForErrorConfirm");
        }
        errorDialog.CloseDialog();
        State = GameState.SelectingSerialDevicePath;
        serialDeviceSelectionDialog.OpenDialog();
    }

    void Update() {

        switch (State) {
            case GameState.SelectingSerialDevicePath: {
                if (Input.GetButtonDown("Cancel")) {
                    SceneManager.LoadScene("Main");
                    break;
                }
                break;
            }
            case GameState.WaitingForMessage: {
                if (Input.GetButtonDown("Cancel")) {
                    CancelConnect();
                    break;
                }
                SimulationMessage message;
                if (thread.GetException() != null) {
                    Debug.Log(thread.GetException());
                    DisplayConnectError(thread.GetException());
                } else if (thread.queue.TryPeek(out message)) {
                    // Ugh what a mess.
                    State = GameState.Running;
                    waitingForMessageDialog.CloseDialog();
                }
                break;
            }
            case GameState.WaitingForErrorConfirm: {
                if (Input.GetButtonDown("Cancel")) {
                    ConfirmError();
                    break;
                }
                break;
            }
            case GameState.Running: {
                if (Input.GetButtonDown("Cancel")) {
                    StopAndWaitForSerialThread();
                    State = GameState.SelectingSerialDevicePath;
                    serialDeviceSelectionDialog.OpenDialog();
                    break;
                }
                bicycleStateProvider.EmitMessagesPerSecond(thread.queue.Count/Time.deltaTime);

                SimulationMessage message = null;
                SimulationMessage latest_pose_message = null;
                SimulationMessage latest_model_message = null;
                while (thread.queue.TryDequeue(out message)) {
                    if (message.pose != null) { latest_pose_message = message; }
                    if (message.model != null) { latest_model_message =  message; }
                }

                if (latest_pose_message != null) {
                    bicycleStateProvider.Emit(latest_pose_message);
                }

                if (latest_model_message != null) {
                    bicycleStateProvider.EmitVelocity(latest_model_message.model.v);
                }
                break;
            }
        }
    }

    void OnDestroy() {
        StopAndWaitForSerialThread();
    }
}

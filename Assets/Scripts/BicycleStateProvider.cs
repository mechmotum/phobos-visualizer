﻿using UnityEngine;

public class BicycleStateProvider : MonoBehaviour {
    public delegate void StateChangeHandler(BicycleState value);
    public event StateChangeHandler OnStateChange;
    public void Emit(BicycleState value) {
        OnStateChange(value);
    }

    public delegate void VelocityChangeHandler(float value);
    public event VelocityChangeHandler OnVelocityChange;
    public void EmitVelocity(float value) {
        OnVelocityChange(value);
    }

    public delegate void MessagesPerSecondChangeHandler(float value);
    public event MessagesPerSecondChangeHandler OnMessagesPerSecondChange;
    public void EmitMessagesPerSecond(float value) {
        OnMessagesPerSecondChange(value);
    }
}

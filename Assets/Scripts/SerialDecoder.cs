﻿using System.Collections.Generic;
using UnityEngine;

/// <summary>
/// This class provides the standard COBS and Protobuf decoding code. A user has
/// to provide the data through the exposed buffers and then call ProcessReadBuffer().
/// </summary>
public class SerialDecoder {
	private byte[] _readBuffer = new byte[4096];
	private byte[] _messageBuffer = new byte[4096];

	public byte[] ReadBuffer { get { return _readBuffer; } }

	private int _readBufferOffset = 0;

	public int ReadBufferOffset {
		get { return _readBufferOffset; }
		set { _readBufferOffset = value; }
	}

	public int ReadBufferRemainingCapacity { get { return ReadBuffer.Length - ReadBufferOffset; } }

	public IEnumerable<pb.SimulationMessage> ProcessReadBuffer() {
		List<pb.SimulationMessage> list = new List<pb.SimulationMessage>();

		int readBufferStart = 0;
		while (readBufferStart < _readBufferOffset) {
			ConsistentOverheadByteStuffing.DecodeResult result = ConsistentOverheadByteStuffing.decode(
				_readBuffer,
				readBufferStart,
				_readBufferOffset,
				_messageBuffer,
				0,
				_messageBuffer.Length
			);

			switch (result.status) {
				case ConsistentOverheadByteStuffing.DecodeResult.Status.OK: {
					readBufferStart += result.consumed;
					// Decoded result.consumed bytes into result.produced bytes.
					using (var message_stream = new System.IO.MemoryStream(_messageBuffer, 0, result.produced, false)) {
						// We have no use for the message size, but have to read it off the start of the stream anyway.
						try {
							ProtoBuf.ProtoReader.DirectReadVarintInt32(message_stream);
							var message = ProtoBuf.Serializer.Deserialize<pb.SimulationMessage>(message_stream);
							list.Add(message);
						} catch (ProtoBuf.ProtoException exception) {
							Debug.LogError(exception);
						}
					}
					continue;
				}
				case ConsistentOverheadByteStuffing.DecodeResult.Status.WRITE_OVERFLOW: {
					Debug.LogError("Package too large to decode.");
					break;
				}
				case ConsistentOverheadByteStuffing.DecodeResult.Status.READ_OVERFLOW: {
					// Hitting a read overflow is usually not a problem. It just
					// means that the encoded data is not fully available yet. If
					// however we were decoding using the entire buffer and still
					// hit a read overflow...
					if (readBufferStart == 0 && _readBufferOffset == _readBuffer.Length) {
						// ...we need a bigger read_buffer.
						Debug.LogError("Encoded package does not fit in read buffer.");
					}
					break;
				}
				case ConsistentOverheadByteStuffing.DecodeResult.Status.UNEXPECTED_ZERO: {
					readBufferStart += result.consumed;
					Debug.LogWarning("Unexpected zero, resuming decoding at next byte.");
					continue;
				}
				default: {
					Debug.LogError("Unknown DecodeResult.Status");
					break;
				}
			}
			// Break the loop by default.
			break;
		}

		// If we processed any frames.
		if (readBufferStart > 0) {
			// Shove remaining bytes in buffer back to the beginning.
			for (int bi = 0, fi = readBufferStart; fi < _readBufferOffset; ++bi, ++fi) {
				_readBuffer[bi] = _readBuffer[fi];
			}

			// Make sure new data is written after these left-over bytes.
			_readBufferOffset = _readBufferOffset - readBufferStart;
		}

		return list;
	}
}

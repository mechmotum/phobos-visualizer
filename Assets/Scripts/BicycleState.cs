﻿using Units.Angles;
using pb;

[System.Serializable]
public class BicycleState {
	public float X;
	public float Y;
	public Radians Roll;
	public Radians Pitch;
	public Radians Yaw;
	public Radians Steer;
	public Radians Wheel;

	public static implicit operator BicycleState(SimulationMessage message) {
		if (message.pose == null) return null;

		return new BicycleState {
			X = message.pose.x,
			Y = message.pose.y,
			Roll = message.pose.roll,
			Pitch = message.pose.pitch,
			Yaw = message.pose.yaw,
			Steer = message.pose.steer,
			Wheel = message.pose.rear_wheel,
		};
	}
}

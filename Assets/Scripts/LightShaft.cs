﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

[RequireComponent(typeof(MeshRenderer))]
public class LightShaft : MonoBehaviour {

	MeshRenderer meshRenderer;

	[SerializeField] Transform player;
	[SerializeField] Transform target;

	public Transform Target { get { return target; } set { target = value; } }

	void Awake() {
		meshRenderer = GetComponent<MeshRenderer>();
	}
	
	void FixedUpdate() {
		transform.position = Vector3.Lerp(transform.position, target.position, Time.fixedDeltaTime*2.0f);
	}

	void Update () {
		Vector2 a = new Vector2(transform.position.x, transform.position.z);
		Vector2 b = new Vector2(player.position.x, player.position.z);
		meshRenderer.material.color = new Color(
			meshRenderer.material.color.r,
			meshRenderer.material.color.g,
			meshRenderer.material.color.b,
			Mathf.InverseLerp(2, 15, Vector2.Distance(a, b))*0.2f
		);
	}
}

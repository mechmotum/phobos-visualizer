public class ConcurrentBoundedQueue<T> {
    private T[] items;
    private int enqueueIndex = 0;
    private int dequeueIndex = 0;
    private int count = 0;

    private bool ThreadUnsafeIsEmpty { get { return count == 0; } }

    private bool ThreadUnsafeIsFull { get { return count == items.Length; } }

    // Invariant dequeueIndex + count % items.Length == enqueueIndex.

    public ConcurrentBoundedQueue(int capacity) {
        this.items = new T[capacity];
    }

    private int NextIndex(int index) {
        return index == items.Length - 1 ? 0 : index + 1;
    }

    private void ThreadUnsafeUncheckedEnqueue(T item) {
        // Set an item.
        items[enqueueIndex] = item;

        // Update enqueueIndex and increase the count.
        enqueueIndex = NextIndex(enqueueIndex);
        count += 1;

        // Signal all waiting threads to reevaluate their waiting conditions.
        System.Threading.Monitor.PulseAll(items);
    }

    private void ThreadUnsafeUncheckedDequeue(out T item) {
        // Take an item and delete its reference. 
        item = items[dequeueIndex];
        items[dequeueIndex] = default(T);

        // Update dequeueIndex and decrease the count.
        dequeueIndex = NextIndex(dequeueIndex);
        count -= 1;

        // Signal all waiting threads to reevaluate their waiting conditions.
        System.Threading.Monitor.PulseAll(items);
    }

    // Try to enqueue an item. Returns false if the queue is full.
    public bool TryEnqueue(T item) {
        lock (items) {
            if (ThreadUnsafeIsFull) return false;
            ThreadUnsafeUncheckedEnqueue(item);
            return true;
        }
    }

    // Wait until there is space for the item in the queue and enqueue it.
    public void Enqueue(T item) {
        lock (items) {
            while (ThreadUnsafeIsFull) System.Threading.Monitor.Wait(items);
            ThreadUnsafeUncheckedEnqueue(item);
        }
    }

    // Try looking at an item in the queue without dequeueing it. Returns false when the queue is empty.
    public bool TryPeek(out T item) {
        lock (items) {
            if (ThreadUnsafeIsEmpty) {
                item = default(T);
                return false;
            }
            item = items[dequeueIndex];
            return true;
        }
    }

    // Wait until we can look at an item in the queue without dequeuing it.
    public void Peek(out T item) {
        lock (items) {
            while (ThreadUnsafeIsEmpty) System.Threading.Monitor.Wait(items);
            item = items[dequeueIndex];
        }
    }

    // Try dequeueing an item from the queue. Returns false whent he queue is empty.
    public bool TryDequeue(out T item) {
        lock (items) {
            if (ThreadUnsafeIsEmpty) {
                item = default(T);
                return false;
            }
            ThreadUnsafeUncheckedDequeue(out item);
            return true;
        }
    }

    public void Dequeue(out T item) {
        lock (items) {
            while (ThreadUnsafeIsEmpty) System.Threading.Monitor.Wait(items);
            ThreadUnsafeUncheckedDequeue(out item);
        }
    }

    public int Count { get {
        lock (items) return count;
    } }
}

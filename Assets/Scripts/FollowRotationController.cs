﻿using UnityEngine;

class FollowRotationController : MonoBehaviour {
    [SerializeField]
    GameObject target;

    [SerializeField]
    Toggle3 toggle = new Toggle3(true, true, true);

    Vector3 initial;
    Quaternion relative;

	void Awake() {
        initial = transform.eulerAngles;
        relative = Quaternion.Inverse(target.transform.rotation) * transform.rotation;
    }

    // Use LateUpdate so the target transform has a chance to update first. 
    void LateUpdate() {
		Quaternion rotation = target.transform.rotation * relative;
        // Apply the new rotations for the enabled euler angles, use the original
        // rotation otherwise.
		transform.rotation = Quaternion.Euler(toggle.Choose(rotation.eulerAngles, initial));
	}
}
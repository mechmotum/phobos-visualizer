namespace Units.Angles {

    public abstract class Constants {
        public const float RadiansPerRevolution = (float) (2*System.Math.PI);
        public const float RevolutionsPerRadian = 1/RadiansPerRevolution;
        public const float DegreesPerRevolution = 360;
        public const float RevolutionsPerDegree = 1/DegreesPerRevolution;
        public const float RadiansPerDegree = RadiansPerRevolution/DegreesPerRevolution;
        public const float DegreesPerRadian = DegreesPerRevolution/RadiansPerRevolution;
    }

    public interface IAngularUnit {
        Degrees ToDegrees { get; }
        Radians ToRadians { get; }
        Revolutions ToRevolutions { get; }
    }

    public struct Degrees : IAngularUnit {
        private float value { get; }
        public Degrees ToDegrees { get { return this; } }
        public Radians ToRadians { get { return this; } }
        public Revolutions ToRevolutions { get { return this; } }
        private Degrees(float degrees) { this.value = degrees; }
        public static implicit operator Degrees(float degrees) { return new Degrees(degrees); }
        public static explicit operator float(Degrees degrees) { return degrees.value; }
        public static implicit operator Degrees(Radians radians) { return ((float) radians)*Constants.DegreesPerRadian; }
        public static implicit operator Degrees(Revolutions revolutions) { return ((float) revolutions)*Constants.DegreesPerRevolution; }
        public static Degrees operator +(Degrees degrees) { return +degrees.value; }
        public static Degrees operator -(Degrees degrees) { return -degrees.value; }
        public static Degrees operator +(Degrees lhs, Degrees rhs) { return lhs.value + rhs.value; }
        public static Degrees operator -(Degrees lhs, Degrees rhs) { return lhs.value - rhs.value; }
        public static Degrees operator *(Degrees lhs, float rhs) { return lhs.value*rhs; }
        public static Degrees operator *(float lhs, Degrees rhs) { return lhs*rhs.value; }
        public static Degrees operator /(Degrees lhs, float rhs) { return lhs.value/rhs; }
        public static Degrees operator /(float lhs, Degrees rhs) { return lhs/rhs.value; }
        public static Degrees operator %(Degrees lhs, Degrees rhs) { return lhs.value % rhs.value; }
        public static bool operator ==(Degrees lhs, Degrees rhs) { return lhs.value == rhs.value; }
        public static bool operator !=(Degrees lhs, Degrees rhs) { return lhs.value != rhs.value; }
        public override bool Equals(object obj) { return obj is Degrees && this == (Degrees) obj; }
        public override int GetHashCode() { return value.GetHashCode(); }
        public static bool operator >(Degrees lhs, Degrees rhs) { return lhs.value > rhs.value; }
        public static bool operator <(Degrees lhs, Degrees rhs) { return lhs.value < rhs.value; }
        public static bool operator >=(Degrees lhs, Degrees rhs) { return lhs.value >= rhs.value; }
        public static bool operator <=(Degrees lhs, Degrees rhs) { return lhs.value <= rhs.value; }
    }

    public struct Radians : IAngularUnit {
        private float value { get; }
        public Degrees ToDegrees { get { return this; } }
        public Radians ToRadians { get { return this; } }
        public Revolutions ToRevolutions { get { return this; } }
        private Radians(float radians) { this.value = radians; }
        public static implicit operator Radians(float radians) { return new Radians(radians); }
        public static explicit operator float(Radians radians) { return radians.value; }
        public static implicit operator Radians(Degrees degrees) { return ((float) degrees)*Constants.RadiansPerDegree; }
        public static implicit operator Radians(Revolutions revolutions) { return ((float) revolutions)*Constants.RadiansPerRevolution; }
        public static Radians operator +(Radians radians) { return +radians.value; }
        public static Radians operator -(Radians radians) { return -radians.value; }
        public static Radians operator +(Radians lhs, Radians rhs) { return lhs.value + rhs.value; }
        public static Radians operator -(Radians lhs, Radians rhs) { return lhs.value - rhs.value; }
        public static Radians operator *(Radians lhs, float rhs) { return lhs.value*rhs; }
        public static Radians operator *(float lhs, Radians rhs) { return lhs*rhs.value; }
        public static Radians operator /(Radians lhs, float rhs) { return lhs.value/rhs; }
        public static Radians operator /(float lhs, Radians rhs) { return lhs/rhs.value; }
        public static Radians operator %(Radians lhs, Radians rhs) { return lhs.value % rhs.value; }
        public static bool operator ==(Radians lhs, Radians rhs) { return lhs.value == rhs.value; }
        public static bool operator !=(Radians lhs, Radians rhs) { return lhs.value != rhs.value; }
        public override bool Equals(object obj) { return obj is Radians && this == (Radians) obj; }
        public override int GetHashCode() { return value.GetHashCode(); }
        public static bool operator >(Radians lhs, Radians rhs) { return lhs.value > rhs.value; }
        public static bool operator <(Radians lhs, Radians rhs) { return lhs.value < rhs.value; }
        public static bool operator >=(Radians lhs, Radians rhs) { return lhs.value >= rhs.value; }
        public static bool operator <=(Radians lhs, Radians rhs) { return lhs.value <= rhs.value; }
    }

    public struct Revolutions : IAngularUnit {
        private float value { get; }
        public Degrees ToDegrees { get { return this; } }
        public Radians ToRadians { get { return this; } }
        public Revolutions ToRevolutions { get { return this; } }
        private Revolutions(float revolutions) { this.value = revolutions; }
        public static implicit operator Revolutions(float revolutions) { return new Revolutions(revolutions); }
        public static explicit operator float(Revolutions revolutions) { return revolutions.value; }
        public static implicit operator Revolutions(Degrees degrees) { return ((float) degrees)*Constants.RevolutionsPerDegree; }
        public static implicit operator Revolutions(Radians radians) { return ((float) radians)*Constants.RevolutionsPerRadian; }
        public static Revolutions operator +(Revolutions revolutions) { return +revolutions.value; }
        public static Revolutions operator -(Revolutions revolutions) { return -revolutions.value; }
        public static Revolutions operator +(Revolutions lhs, Revolutions rhs) { return lhs.value + rhs.value; }
        public static Revolutions operator -(Revolutions lhs, Revolutions rhs) { return lhs.value - rhs.value; }
        public static Revolutions operator *(Revolutions lhs, float rhs) { return lhs.value*rhs; }
        public static Revolutions operator *(float lhs, Revolutions rhs) { return lhs*rhs.value; }
        public static Revolutions operator /(Revolutions lhs, float rhs) { return lhs.value/rhs; }
        public static Revolutions operator /(float lhs, Revolutions rhs) { return lhs/rhs.value; }
        public static Revolutions operator %(Revolutions lhs, Revolutions rhs) { return lhs.value % rhs.value; }
        public static bool operator ==(Revolutions lhs, Revolutions rhs) { return lhs.value == rhs.value; }
        public static bool operator !=(Revolutions lhs, Revolutions rhs) { return lhs.value != rhs.value; }
        public override bool Equals(object obj) { return obj is Revolutions && this == (Revolutions) obj; }
        public override int GetHashCode() { return value.GetHashCode(); }
        public static bool operator >(Revolutions lhs, Revolutions rhs) { return lhs.value > rhs.value; }
        public static bool operator <(Revolutions lhs, Revolutions rhs) { return lhs.value < rhs.value; }
        public static bool operator >=(Revolutions lhs, Revolutions rhs) { return lhs.value >= rhs.value; }
        public static bool operator <=(Revolutions lhs, Revolutions rhs) { return lhs.value <= rhs.value; }
    }
}
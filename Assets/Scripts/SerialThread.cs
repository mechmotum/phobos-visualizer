﻿using System;
using System.IO;
using System.IO.Compression;
using System.IO.Ports;
using System.Threading;

public class SerialThread {
	Thread thread;

	volatile bool running = true;

	volatile string serialDevicePort;

	volatile Exception exception;

	public ConcurrentBoundedQueue<pb.SimulationMessage> queue = new ConcurrentBoundedQueue<pb.SimulationMessage>(100);

	public SerialThread(string serialDevicePort) {
		this.serialDevicePort = serialDevicePort;

		thread = new Thread(Main);
		thread.Name = "SerialThread";
		thread.Start();
	}

	/// <summary>
	/// Signal the thread to stop. The thread will not stop immediately and might
	/// not stop at all, depending on the implementation.
	/// </summary>
	public void SignalStop() {
		running = false;
	}

	public void WaitUntilStopped() {
		thread.Join();
	}

	public bool IsRunning() {
		return thread.IsAlive;
	}

	public Exception GetException() {
		return exception;
	}

	void Main() {
		try {
			using (FileStream file = new FileStream(
				string.Format("{0}.pb.cobs.gz", DateTime.UtcNow.ToString("yyyy-MM-dd'T'HH'h'mm'm'ss'sZ'")),
				FileMode.CreateNew,
				FileAccess.Write
			))
			using (GZipStream gzip = new GZipStream(file, CompressionMode.Compress))
			using (SerialPort port = new SerialPort(serialDevicePort)) {
				port.BaudRate = 115200;
				port.Parity = Parity.None;
				port.Handshake = Handshake.None;
				port.DataBits = 8;
				port.StopBits = StopBits.One;
				port.ReadTimeout = 10;

				port.Open();
				
				SerialDecoder decoder = new SerialDecoder();

				while (running) {
					try {
						// Read as many new bytes as possible, appending to the read buffer. 
						int bytesRead = port.Read(decoder.ReadBuffer, decoder.ReadBufferOffset, decoder.ReadBufferRemainingCapacity);
						
						if (bytesRead == 0) {
							Thread.Sleep(0);
							continue;
						}

						// Write read bytes to output file.
						gzip.Write(decoder.ReadBuffer, decoder.ReadBufferOffset, bytesRead);

						// Update the read buffer size.
						decoder.ReadBufferOffset += bytesRead;
						
						foreach (var message in decoder.ProcessReadBuffer()) {
							// Discards newer messages. TODO: Implement OverwritingEnqueue. 
							queue.TryEnqueue(message);
						}
					} catch (TimeoutException) {
						Thread.Sleep(0);
						continue;
					}
				}
			}
		}
		catch (Exception exception) {
			this.exception = exception;
		}
	}
}

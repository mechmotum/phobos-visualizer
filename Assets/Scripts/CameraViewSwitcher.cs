﻿using UnityEngine;

public class CameraViewSwitcher : MonoBehaviour {

	[SerializeField] Transform[] transforms;
	[SerializeField] int transformIndex;
	[SerializeField] float lerpSpeed = 8.0f;

#if UNITY_EDITOR
	void OnValidate() {
		Transform target = transforms[transformIndex];
		transform.position = target.position;
	}
#endif

	public void RotateView() {
		transformIndex += 1;
		if (transformIndex >= transforms.Length) transformIndex = 0;
	}

	void Update() {
		if (Input.GetKeyDown(KeyCode.V)) RotateView();
		Transform target = transforms[transformIndex];
		transform.position = Vector3.Lerp(transform.position, target.position, Time.deltaTime*lerpSpeed);
		// Because of `CameraController` we cannot use the rotation. 
		// transform.rotation = Quaternion.Lerp(transform.rotation, target.rotation, Time.deltaTime*lerpSpeed);
	}
}

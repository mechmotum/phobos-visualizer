﻿using UnityEngine;
using UnityEngine.SceneManagement;
using Units.Angles;

public class TestScene : MonoBehaviour {

	[SerializeField] BicycleStateProvider bicycleStateProvider;

	BicycleState state = new BicycleState {
		X = 0,
		Y = 0,
		Yaw = 0,
		Pitch = (Degrees) 18,
		Roll = 0,
		Steer = 0,
		Wheel = 0,
	};

	float steerSpeed = 1;
	float steerToRoll = 0.8f;
	float velocity = 0;
	float magicWindConstant = -0.010f;
	float magicFrictionConstant = -0.20f;
	float acceleration = 4.0f;
	float brakeAcceleration = 6.0f;
	float reverseAcceleration = 0.8f;

	void Update () {
    if (Input.GetButtonDown("Cancel")) { SceneManager.LoadScene("Main"); }

		// It is harder to turn the steer when going faster. This magic scalar is used
		// to simulate this effect. Figured y = 1/(|x|/s + 1) might work well for some
		// scalar s.
		float magicSteerScaler = 1/(Mathf.Abs(velocity)/20 + 1);

		state.Steer += Input.GetAxisRaw("Horizontal")*magicSteerScaler*steerSpeed*Time.deltaTime;

		float dv = Input.GetAxisRaw("Vertical");
		velocity += dv*(dv >= 0 ? acceleration : (velocity > 0 ? brakeAcceleration : reverseAcceleration))*Time.deltaTime;

		if (Input.GetKey(KeyCode.Space)) {
			velocity += Time.deltaTime*acceleration;
			state.Pitch = Mathf.Lerp((float) state.Pitch, (float) (Radians) (Degrees) 50, Time.deltaTime*5);
		} else {
			state.Pitch -= Time.deltaTime*0.8f;
			if ((Degrees) state.Pitch < 18) state.Pitch = (Degrees) 18;
		}

		// Limit steer.
		if ((Degrees) state.Steer < -180) state.Steer = (Degrees) (-180);
		if ((Degrees) state.Steer >= 180) state.Steer = (Degrees) 180;

		// Compute roll from steer.
		state.Roll = state.Steer*steerToRoll;

		// Compute yaw from steer.
		state.Yaw += Time.deltaTime*state.Steer;

		// Update position.
		state.X += Time.deltaTime*velocity*Mathf.Cos((float) (Radians) state.Yaw);
		state.Y += Time.deltaTime*velocity*Mathf.Sin((float) (Radians) state.Yaw);

		// Update velocity.
		velocity += Time.deltaTime*magicWindConstant*velocity*velocity;
		velocity += Time.deltaTime*magicFrictionConstant*velocity;
		if (velocity < -2) velocity = -2;

		bicycleStateProvider.Emit(state);
		bicycleStateProvider.EmitVelocity(velocity);
	}
}

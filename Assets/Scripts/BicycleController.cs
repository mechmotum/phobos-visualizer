using UnityEngine;
using pb;
using Units.Angles;

class BicycleController : MonoBehaviour {

    [SerializeField] BicycleStateProvider bicycleStateProvider;

    [SerializeField] GameObject rearWheel;

    [SerializeField] GameObject frontFrame;

    [SerializeField] GameObject frontWheel;

    public void OnEnable() {
        bicycleStateProvider.OnStateChange += SetBicycleState;
    }

    public void OnDisable() {
        bicycleStateProvider.OnStateChange -= SetBicycleState;
    }

    // An implicit cast exists but that apparently doesn't work for event
    // handlers.
    public void SetBicycleState(BicycleState state) {
        SetBicycleState((BicycleStateUnity) state);
    }

    public void SetBicycleState(BicycleStateUnity state) {
        transform.localPosition = state.Position;
        transform.localRotation = state.Rotation;

        // Update wheel angles.
        rearWheel.transform.localRotation = Quaternion.Euler(0.0f, (float) state.Wheel, 0.0f);
        frontWheel.transform.localRotation = Quaternion.Euler(0.0f, (float) state.Wheel, 0.0f);

        // Update front frame angle.
        frontFrame.transform.localRotation = Quaternion.Euler(0.0f, 0.0f, (float) state.Steer);
    }
}

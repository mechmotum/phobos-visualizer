using UnityEngine;

class FollowPositionController : MonoBehaviour {
    [SerializeField]
    GameObject target;

    [SerializeField]
    Toggle3 follow = new Toggle3(true, true, true);

    Vector3 initial;
    Vector3 relative;

    void Start() {
        initial = transform.position;
        relative = transform.position - target.transform.position;
    }

    // Use LateUpdate so that the target transform is updated.
    void LateUpdate() {
        Vector3 position = target.transform.position + relative;
        // Apply the new position for the enabled dimensions, use the original
        // position otherwise.
        transform.position = follow.Choose(position, initial);
    }
}
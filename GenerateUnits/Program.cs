﻿using System.Collections.Generic;
using static System.Console;

namespace GenerateUnits {
    struct Unit {
        public string ClassName;
        public string ArgumentName => ClassName.ToLower();
        public Dictionary<string, string> ClassNameToConversionConstant;
    }

    class Program {
        static void Main(string[] args) {
            Unit[] units = new Unit[] {
                new Unit {
                    ClassName = "Degrees",
                    ClassNameToConversionConstant = new Dictionary<string, string>() {
                        { "Radians", "DegreesPerRadian" },
                        { "Revolutions", "DegreesPerRevolution" },
                    },
                },
                new Unit {
                    ClassName = "Radians",
                    ClassNameToConversionConstant = new Dictionary<string, string>() {
                        { "Degrees", "RadiansPerDegree" },
                        { "Revolutions", "RadiansPerRevolution" },
                    },
                },
                new Unit {
                    ClassName = "Revolutions",
                    ClassNameToConversionConstant = new Dictionary<string, string>() {
                        { "Degrees", "RevolutionsPerDegree" },
                        { "Radians", "RevolutionsPerRadian" },
                     },
                },
            };
            Write(
                "namespace Units.Angles {\n" +
                "\n" +
                "    public abstract class Constants {\n" +
                "        public const float RadiansPerRevolution = (float) (2*System.Math.PI);\n" + 
                "        public const float RevolutionsPerRadian = 1/RadiansPerRevolution;\n" + 
                "        public const float DegreesPerRevolution = 360;\n" + 
                "        public const float RevolutionsPerDegree = 1/DegreesPerRevolution;\n" + 
                "        public const float RadiansPerDegree = RadiansPerRevolution/DegreesPerRevolution;\n" + 
                "        public const float DegreesPerRadian = DegreesPerRevolution/RadiansPerRevolution;\n" + 
                "    }\n" + 
                "\n" +
                "    public interface IAngularUnit {\n" + 
                "        Degrees ToDegrees { get; }\n" + 
                "        Radians ToRadians { get; }\n" + 
                "        Revolutions ToRevolutions { get; }\n" + 
                "    }\n"
            );
            foreach (Unit unit in units) {
                WriteLine();
                WriteLine("    public struct {0} : IAngularUnit {{", unit.ClassName);
                WriteLine("        private float value { get; }");
                // Interface implementation.
                foreach (Unit innerUnit in units) {
                    WriteLine("        public {0} To{0} {{ get {{ return this; }} }}", innerUnit.ClassName);
                }
                // Constructor and conversion between floats.
                WriteLine("        private {0}(float {1}) {{ this.value = {1}; }}", unit.ClassName, unit.ArgumentName);
                WriteLine("        public static implicit operator {0}(float {1}) {{ return new {0}({1}); }}", unit.ClassName, unit.ArgumentName);
                WriteLine("        public static explicit operator float({0} {1}) {{ return {1}.value; }}", unit.ClassName, unit.ArgumentName);
                // Conversion operators from other units.
                foreach (Unit otherUnit in units) {
                    if (unit.ClassName == otherUnit.ClassName) continue;
                    string conversionConstant;
                    if (!unit.ClassNameToConversionConstant.TryGetValue(otherUnit.ClassName, out conversionConstant)) {
                        throw new System.Exception(string.Format("No conversion constant between {0} and {1}", unit.ClassName, otherUnit.ClassName));
                    }
                    WriteLine("        public static implicit operator {0}({1} {2}) {{ return ((float) {2})*Constants.{3}; }}", unit.ClassName, otherUnit.ClassName, otherUnit.ArgumentName, conversionConstant);
                }
                Write(
                    // Arithmethic operators.
                    "        public static {0} operator +({0} {1}) {{ return +{1}.value; }}\n" +
                    "        public static {0} operator -({0} {1}) {{ return -{1}.value; }}\n" +
                    "        public static {0} operator +({0} lhs, {0} rhs) {{ return lhs.value + rhs.value; }}\n" +
                    "        public static {0} operator -({0} lhs, {0} rhs) {{ return lhs.value - rhs.value; }}\n" +
                    "        public static {0} operator *({0} lhs, float rhs) {{ return lhs.value*rhs; }}\n" +
                    "        public static {0} operator *(float lhs, {0} rhs) {{ return lhs*rhs.value; }}\n" +
                    "        public static {0} operator /({0} lhs, float rhs) {{ return lhs.value/rhs; }}\n" +
                    "        public static {0} operator /(float lhs, {0} rhs) {{ return lhs/rhs.value; }}\n" +
                    "        public static {0} operator %({0} lhs, {0} rhs) {{ return lhs.value % rhs.value; }}\n" +
                    // Comparison operators.
                    "        public static bool operator ==({0} lhs, {0} rhs) {{ return lhs.value == rhs.value; }}\n" +
                    "        public static bool operator !=({0} lhs, {0} rhs) {{ return lhs.value != rhs.value; }}\n" +
                    "        public override bool Equals(object obj) {{ return obj is {0} && this == ({0}) obj; }}\n" +
                    "        public override int GetHashCode() {{ return value.GetHashCode(); }}\n" +
                    "        public static bool operator >({0} lhs, {0} rhs) {{ return lhs.value > rhs.value; }}\n" +
                    "        public static bool operator <({0} lhs, {0} rhs) {{ return lhs.value < rhs.value; }}\n" +
                    "        public static bool operator >=({0} lhs, {0} rhs) {{ return lhs.value >= rhs.value; }}\n" +
                    "        public static bool operator <=({0} lhs, {0} rhs) {{ return lhs.value <= rhs.value; }}\n"
                    , unit.ClassName, unit.ArgumentName
                );
                WriteLine("    }");
            }
            WriteLine("}");
        }
    }
}
